-------------------------------
															Data Validation Tool(DVT)
														------------------------------------
Data Validation Tool(DVT) is a PHP Bank application which helps bank to various methods to validate the members of the bank. DVT is used to compare the EFT file with Account file.
Account file is the text file contains all the customer’s account details in the bank which includes millions of records as bank have millions of customers. EFT file is the excel file 
which contain salary details of the customers at the particular organization. The salary will only process if the customers details in the EFT files are correct with account file.
The new application helps the bank to process the salaries of the members by quick verifying the account details.

The DVT project is divided into four functionality module.These are administration module, Upload file module, report module & search module, 

Administration module is the main module which have all privilleges for the project role.

Upload file module is mainly coded and setup for uploading the Account file and EFT file for validation.

Report module include the report generation of the user status and consolidate report generation.

Search module for filtering the report based on the employe id, name etc..
  
The Data Validation project developed in PHP with Codeigniter framework. Codeigniter framework are light wight and higlly secure which follow the file structure Model, Views, Controllers(MVC). 

Model contain files that are included database quary for the project.

View contain files that are included user interface view( HTML quary) 

Controllers is the intermediater between views and model which call the function from views and model.  
  
     ------------------------------

     Controllers included files are:

     ------------------------------

                welcome.php,
                home.php,
                upload.php,
                consoldidate.php,
                search.php

     -------------------------------

     Model included files are:

     -------------------------------  

                core.php,
                home_model.php,
                upload_model.php,
                consoldidate_model.php,
                search_model.php,

      -------------------------------

      Views included files are:

      -------------------------------

                account_form.php,
                change_pass.php,
                configure_app.php,
                consoldidate_summary.php,
                eft_form.php,
                footer.php,
                force_pay.php,
                header.php,
                home.php,
                passed.php,
                search_view.php,
                summary_all.php,
                test.php,
                upload_summary.php,
                user_add.php,
                user_category.php,
                user_category_editor.php,
                user_list.php,
                user_log.php,
                user_resetpass.php,
                userlogPDFfile.php,
                users.php,
                users_editor.php,
                users_view.php,
                view_pdf_user.php,
                viewPDFfile.php,